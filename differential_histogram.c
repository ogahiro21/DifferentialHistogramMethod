#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "def.h"
#include "var.h"
#include "bmpfile.h"

#define SIZE 256
#define LOCAL 7

void DifferentialHisto(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                       Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH]);

// sobel filter処理する関数
int sobel(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
             int x, int y);

// binarization
void binarization(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                  Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH],
                  int threashold);

int main(int argc, char *argv[])
{
  imgdata idata;

  if (argc < 3) {
    printf("使用法：cpbmp コピー元.bmp コピー先.bmp\n");
  }
  else {
    if (readBMPfile(argv[1], &idata) > 0){
      printf("指定コピー元ファイル%sが見つかりません\n",argv[1]);
    }
    else {
      /* Variable threshold method */
      DifferentialHisto(idata.source, idata.results);
    }
    if (writeBMPfile(argv[2], &idata) > 0){
      printf("コピー先ファイル%sに保存できませんでした\n",argv[2]);
    }
  }
}

void DifferentialHisto(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                       Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH])
{
  int histo[SIZE];
  int x,y;
  int threshold = 0, value = 0;

  // Init histo
  for (y = 0; y < SIZE; y++) {
    histo[y] = 0;
  }

  // Create histogram
  for (y = 1; y < SIZE-1; y++) {
    for (x = 1; x < SIZE-1; x++) {
      value = sobel(source, x, y);
      if(value > 30){
        histo[source[RED][y][x]]+=value;
      }
    }
  }

  // Search maximu
  for (y = 0; y < SIZE; y++) {
    if(histo[threshold] < histo[y]) threshold = y;
  }

  printf("threshold = %d\n",threshold);

  // binarization
  binarization(source,results,threshold);
}

int sobel(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
           int x, int y)
{
  char deltaX[3][3] = {{ 1,  0, -1},
                       { 2,  0, -2},
                       { 1,  0, -1}};

  char deltaY[3][3] = {{ 1,  2,  1},
                       { 0,  0,  0},
                       {-1, -2, -1}};

  int    i, j; // for文用の変数
  int    color = RED;
  int    slope = 0;      // 勾配
  double sumX = 0, sumY = 0; // 近傍画素の合計値

  // 空間フィルタ処理
  for (i = -1; i < 2; i++) {
    for (j = -1; j < 2; j++) {
      sumX += deltaX[i+1][j+1] * source[color][y+i][x+j];
      sumY += deltaY[i+1][j+1] * source[color][y+i][x+j];
    }
  }
  // 勾配の計算
  slope = sqrt( pow(sumX,2) + pow(sumY,2) );
  return slope;
}

void binarization(Uchar source[COLORNUM][MAXHEIGHT][MAXWIDTH],
                  Uchar results[COLORNUM][MAXHEIGHT][MAXWIDTH],
                  int threashold)
{
  int x,y,color;

  for (color = 0; color < 3; color++) {
    for(y = 0; y < SIZE; y++){
      for(x = 0; x < SIZE; x++){
        if(source[RED][y][x] >= threashold) results[color][y][x] = 255;
        else results[color][y][x] = 0;
      }
    }
  }
}
