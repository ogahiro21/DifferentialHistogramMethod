# Differential histogram method
微分ヒストグラム法で閾値を求めて画像を2値化する  


# About differential histogram method
微分ヒストグラム法とは各画素について微分の絶対値を求め、その濃度値ごとに累計を取り、 **濃度地に対する微分値総和のヒストグラムを作成する** 。  
ヒストグラムが最大値となる濃度値を閾値とする。  
ここでは微分値を求めるために[Sobelフィルタ](https://gitlab.com/ogahiro21/SobelFilter)を用いる。  
- 入力画像  
![in](https://gitlab.com/ogahiro21/DifferentialHistogramMethod/raw/image/image/in18.jpeg)  
- 出力画像(閾値=133)  
![out](https://gitlab.com/ogahiro21/DifferentialHistogramMethod/raw/image/image/out18.jpeg)  

- **注意**
- ヒストグラムを作成するとき、 **画像領域の端は累計から除外する**  
  - 端は微分値が正確でなく、ヒストグラムに大きな影響を与えるため。

  
- ヒストグラムを作成するとき、 **微分値が30以下のものは累計から除外する**
  - ノイズによる微分値は小さくても画素数が多いと大きな影響を与えるため

## Usage
**コンパイル**
```
gcc -o cpbmp differential_histogram.c bmpfile.o -lm
```
**画像の出力**
```
./cpbmp in18.bmp ans18.bmp
```
